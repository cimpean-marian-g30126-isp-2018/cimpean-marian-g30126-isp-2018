package g30126.cimpean.marian.l5.e1;

public class TestShape {
    public static void main(String[] args) {
        Shape[] shapes=new Shape[4];
        shapes[0]=new Circle("red",true,2.5);
        shapes[1]=new Rectangle(2,5,"blue",false);
        shapes[2]=new Square(4,"red",true);
        shapes[3]=new Shape("green",true);
        for(int i=0;i<4;i++){
        	shapes[i].getArea();
        	shapes[i].getPerimeter();
        }


    }
}
