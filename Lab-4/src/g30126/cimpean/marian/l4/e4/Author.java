package g30126.cimpean.marian.l4.e4;

public class Author {
	private String name , email;
	char gender;
	public Author(String name, String email, char gender){
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	public String getName(){
		return this.name;
	}
	public String getEmail(){
		return this.email;
	}
	public void setEmail(String email){
		this.email=email;
	}
	public void setGender(char gender){
		this.gender=gender;
	}
	public String toString(){
		return "author - " + this.name+"( "+this.gender+") at  "+this.email;			
	}
}
