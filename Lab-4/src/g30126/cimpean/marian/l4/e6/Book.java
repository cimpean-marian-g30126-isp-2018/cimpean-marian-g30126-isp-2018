package g30126.cimpean.marian.l4.e6;

import g30126.cimpean.marian.l4.e4.Author;
import java.util.Arrays;
public class Book {
	private String name;
	private Author[] authorsList = new Author[100];
	private double price;
	private int qtyInStock;
	public Book(String name, Author[] authors, double price){
		this.name=name;
		authorsList=authors;
		this.price=price;
	}
	public Book(String name, Author[] authors, double price,int qtyInStock){
		this.name=name;
		authorsList=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	public String getName(){
		return this.name;
	}
	public Author[] getAuthor(){
		return authorsList;
	}
	public double getPrice(){
		return this.price;
	}
	public void setPrice(double price){
		this.price=price;
	}
	public int qtyInStock(){
		return this.qtyInStock;
	}
	public void setqtyInStock(int qtyInStock){
		this.qtyInStock=qtyInStock;	
	}
	
	public void printAuthors(){
		int poz=0;
		while(this.authorsList[poz]!=null)
		{
			System.out.println(this.authorsList[poz].getName());
			poz++;
		}
	}
	public String toString(){
		int poz=0,count=0;
		while (this.authorsList[poz]!= null)
		{
				count++;
				poz++;
		}
		return this.name + " by " + count + "authors";
		
	}
	
}
