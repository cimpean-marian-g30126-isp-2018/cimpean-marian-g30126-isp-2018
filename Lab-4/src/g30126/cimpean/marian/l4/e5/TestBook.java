package g30126.cimpean.marian.l4.e5;
import g30126.cimpean.marian.l4.e4.*;
public class TestBook {
	public static void main(String[] args){
		Author a=new Author("Alex","alex.popescu@yahoo.com",'m');
		Book b=new Book("Game of Thrones",a,120,10);
		System.out.println("The book's name is "+b.getName()+" by the "+b.getAuthor());
	}
}
