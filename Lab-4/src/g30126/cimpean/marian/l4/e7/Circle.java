package g30126.cimpean.marian.l4.e7;

public class Circle {
	private double radius=1.0;
	private String colour="red";
	public Circle(){}
	public Circle(double radius){
		this.radius=radius;
	}
	public double getRadius(){
		return radius;
	}
	public double getArea(){
		return radius*radius*Math.PI;
	}
	
	
}
