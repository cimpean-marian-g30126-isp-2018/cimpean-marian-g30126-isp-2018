package g30126.cimpean.marian.l4.e7;

public class Cylinder extends Circle {
		private double height=1.0;
		public Cylinder(){
			
		}
		public Cylinder(double radius){
			super(radius);
		}
		
		public Cylinder(double radius,double height){
			super(radius);
			this.height=height;
		}
		public double getHeight(){
			return this.height;
		}
		@Override
		public double getArea()
		{
			return 3.14 * this.getRadius() * this.getRadius();
		}
		
		public double getVolume(){
			return height* this.getArea();
		}
}
