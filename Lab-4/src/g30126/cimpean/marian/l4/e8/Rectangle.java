package g30126.cimpean.marian.l4.e8;

public class Rectangle extends Shape {
		double width, leight;
		public Rectangle(){
			this.width=1.0;
			this.leight=1.0;
		}
		public Rectangle(double width,double leight){
			this.width=width;
			this.leight=leight;
		}
		public Rectangle(double width,double leight,String colour,boolean filled){
			super(colour,filled);
			this.width=width;
			this.leight=leight;
		}
		public double getWidth(){
			return this.width;
		}
		public void setWidtht(double width){
			this.width=width;
		}
		public double getLeight(){
			return this.leight;
		}
		public void setLeight(double width){
			this.leight=leight;
		}
		public double getArea(){
			return this.width*this.leight;
		}
		public double getPerimeter(){
			return 2*(this.width+this.leight);
		}
		@Override
		public String toString(){
			return "A Rectangle with width="+this.width+" and with leight="+this.leight+" is a subclass of "+this.toString();
		}
		
		
		
}
