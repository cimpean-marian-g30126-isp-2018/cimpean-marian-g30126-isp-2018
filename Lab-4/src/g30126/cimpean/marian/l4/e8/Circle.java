package g30126.cimpean.marian.l4.e8;

public class Circle extends Shape {
	private double radius;
	public Circle(){
		this.radius=1.0;
	}
	public Circle(double radius){
		this.radius=radius;
	}
	public Circle(double radius,String colour,boolean filled){
		super(colour,filled);
		this.radius=radius;
	}
	public double getRadius(){
		return this.radius;
	}
	public void setRadius(double radius){
		this.radius=radius;
	}
	public double getArea(){
		return 3.14*this.radius*this.radius;
	}
	public double getPerimeter(){
		return 2*3.14*this.radius;
	}
	@Override
	public String toString(){
		return "A circle with radius =" +this.radius+ " which is a subclass of "+this.toString();
	}
	

}
