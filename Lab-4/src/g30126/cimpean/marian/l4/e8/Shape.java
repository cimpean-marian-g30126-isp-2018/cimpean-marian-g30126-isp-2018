package g30126.cimpean.marian.l4.e8;

public class Shape {
	private String colour="red";
	private boolean filled=true;
	public Shape(){
		this.colour="green";
		this.filled=true;
	}
	public Shape(String colour,boolean filled){
		this.colour=colour;
		this.filled=filled;
	}
	public String getColour(){
		return this.colour;
	}
	public void setColour(String colour){
		this.colour=colour;
	}
	public boolean isFilled(){
		if(this.filled)
			return true;
		else return false;
	}
	public void setFilled(boolean filled){
		this.filled=filled;
	}
	public String toString(){
		String str="";
		if(this.filled==true)
			str+="filled";
		else str+="not filled";
		return "A shape with colour of "+this.colour+" and "+str;
	}

}
