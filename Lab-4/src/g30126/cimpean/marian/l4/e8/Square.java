package g30126.cimpean.marian.l4.e8;

public class Square extends Rectangle {
	public Square(){
		
	}
	public Square(double side){
		super(side,side);
	}
	public Square(double side,String colour,boolean filled){
		super(side,side,colour,filled);
	}
	public double getSide() {
		return getWidth();
	}
	public void setSide(double side) {
		setWidth(side);
	}
	public void setWidth(double side) {
		setWidth(side);
	}
	public void setLength(double side) {
		setLength(side);
	}
	public String toString() {
		super.toString();
		return "A Square with side="+getSide()+",which is a subclass of "+ super.toString();
	}

}
