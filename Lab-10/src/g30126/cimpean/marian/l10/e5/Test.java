package g30126.cimpean.marian.l10.e5;

public class Test {

    public static void main(String[] args){
        Buffer b = new Buffer();
        Producer producer = new Producer(b);
        Consumer c = new Consumer(b);
        Consumer c2 = new Consumer(b);
        //Lanseaza cele 3 fire de executie. Se observa ca cele 3 fire de executie
        // folosesc in comun obiectul b de tip Buffer. Exista un fir producer ce realizeaza adaugarea elementelor in buffer si a doua
        // obiecte pt extragerea elementelor din buffer.
        producer.start();
        c.start();
        c2.start();
    }
}