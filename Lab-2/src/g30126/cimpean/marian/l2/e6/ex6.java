package g30126.cimpean.marian.l2.e6;
import java.util.Scanner;
public class ex6 {
    static void nerecursiv(int n){
    	int s=1;
    	for(int i=1;i<=n;i++)
    		s=i*s;
    	System.out.println("N!= "+s);
    }
    static int recursiv(int n){
    	int result;
        if(n==1)
          return 1;
        result = recursiv(n-1) * n;
        return result;
    }
    public static void main(String[] args){
    	Scanner in = new Scanner(System.in);
    	System.out.println("Valoarea lui N:");
    	int n=in.nextInt();
    	System.out.println("N! calculat nerecursiv: ");
    	nerecursiv(n);
    	int k = recursiv(n);
    	System.out.println("N! calculat recursiv: "+k);
    	
    	
    }
}
