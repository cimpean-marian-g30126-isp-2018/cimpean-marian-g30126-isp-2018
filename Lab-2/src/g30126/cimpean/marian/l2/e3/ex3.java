package g30126.cimpean.marian.l2.e3;
import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti primul capat al intervalului: ");
        int a = scanner.nextInt();
        System.out.println("Introduceti al doilea capat al intervalului: ");
        int b= scanner.nextInt();
        int count = 0;
        for (int i = a; i <= b; i++) {
        	
            boolean ok = true;
            
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    ok = false;
                    break; 
                }
            }

      
            if (ok) {
                count++;
                System.out.print(i + ", ");

            }

        }
        System.out.println("Cate numere prime sunt: " + count);
    }
}