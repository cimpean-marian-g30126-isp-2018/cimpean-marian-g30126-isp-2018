package g30126.cimpean.marian.l7.e4;

public class Definition {
    String definition;

    public Definition(String definition) {
        this.definition=definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    public String getDefinition() {
        return definition;
    }

}
