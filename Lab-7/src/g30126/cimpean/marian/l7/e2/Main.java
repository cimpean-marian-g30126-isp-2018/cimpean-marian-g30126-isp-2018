package g30126.cimpean.marian.l7.e2;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import g30126.cimpean.marian.l7.e1.BankAccount;

public class Main {

    public static void main(String[] args) {
        ArrayList<BankAccount> bankAccounts=new ArrayList<>();
        Bank bank=new Bank(bankAccounts);
        bank.addAccounts("ana", 550);
        bank.addAccounts("mihai", 500);
        bank.printAccounts();
        bank.printAccount(40, 500);
        Collections.sort(bank.getAllAccount(),BankAccount.bComparator);
        bank.printAccounts();

    }

}
