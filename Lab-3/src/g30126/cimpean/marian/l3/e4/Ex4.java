package g30126.cimpean.marian.l3.e4;
import becker.robots.*;
public class Ex4 {
	public static void main(String[] args)
	   {
	// Set up the initial situation
	City ny= new City();// create map
	Wall blockAve0=new Wall(ny,1,2,Direction.WEST);
	Wall blockAve1=new Wall(ny,2,2,Direction.WEST);
	Wall blockAve2=new Wall(ny,1,2,Direction.NORTH);
	Wall blockAve3=new Wall(ny,1,3,Direction.NORTH);
	Wall blockAve4=new Wall(ny,1,3,Direction.EAST);
	Wall blockAve5=new Wall(ny,2,3,Direction.EAST);
	Wall blockAve6=new Wall(ny,2,2,Direction.SOUTH);
	Wall blockAve7=new Wall(ny,2,3,Direction.SOUTH);
	Robot mark = new Robot(ny,0,4,Direction.WEST);
	int i;
	for(i=0;i<3;i++)
	{
		mark.move();
	}
	mark.turnLeft();
	for(i=0;i<3;i++)
	{
		mark.move();
	}
	mark.turnLeft();
	for(i=0;i<3;i++)
	{
		mark.move();
	}
	mark.turnLeft();
	for(i=0;i<3;i++)
	{
		mark.move();
	}
	mark.turnLeft();   
	
	   }
}