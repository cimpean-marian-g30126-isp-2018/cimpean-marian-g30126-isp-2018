package g30126.cimpean.marian.l3.e3;
import becker.robots.*;
public class Ex3 {
	public static void main(String[] args)
	   {
	// Set up the initial situation
	City prague = new City();
   Robot karel = new Robot(prague, 5, 5, Direction.NORTH);//am ales punctul de pornire 5,5 deoarece alegand 1,1 nu vom vedea nimic deoarece "robotul" iasa din cadru 
   for(int i=0;i<5;i++)
   {
	   karel.move();
   }
   karel.turnLeft();
   karel.turnLeft();
   for(int i=0;i<5;i++)
   {
	   karel.move();
   }
}
}