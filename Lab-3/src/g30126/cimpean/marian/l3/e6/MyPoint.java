package g30126.cimpean.marian.l3.e6;

public class MyPoint {
	private int x,y;
	
	public MyPoint(){
	}
	public MyPoint(int x, int y){
		this.x = x;
		this.y = y;
	}
	public int getX(){
		 return x;
	}
	
	public int getY(){
		return y;
	}
	
	public void setX(int setMyX){
		this.x=setMyX;
	}
	
	public void setY(int setMyY){
		this.y=setMyY;
	}
	//@Override
	public String toString(){
		return "("+this.x+","+this.y+")";
	}
	
	public double distance(int x,int y){
		return Math.sqrt((x-this.x)^2+(y-this.y)^2);
	}
	
	public double distance(MyPoint p){
		return Math.sqrt((p.x-this.x)^2+(p.y-this.y)^2);
	}

}
