package g30126.cimpean.marian.l3.e5;
import becker.robots.*;
public class Ex5 {
      public static void main(String[] args)
      {
    	  City ny=new City();
    	  Wall blockAve0=new Wall(ny,1,2,Direction.SOUTH);
    	  Wall blockAve1=new Wall(ny,1,2,Direction.EAST);
    	  Wall blockAve2=new Wall(ny,1,2,Direction.NORTH);
    	  Wall blockAve3=new Wall(ny,1,1,Direction.NORTH);
    	  Wall blockAve4=new Wall(ny,1,1,Direction.WEST);
    	  Wall blockAve5=new Wall(ny,2,1,Direction.WEST);
    	  Wall blockAve6=new Wall(ny,2,1,Direction.SOUTH);
    	  Robot karel= new Robot(ny,1,2,Direction.SOUTH);
    	  Thing bed = new Thing(ny, 2, 2);
    	  karel.turnLeft();
    	  karel.turnLeft();
    	  karel.turnLeft();
    	  karel.move();
    	  karel.turnLeft();
    	  karel.move();
    	  karel.turnLeft();
    	  karel.move();
    	  karel.pickThing();
      }
}